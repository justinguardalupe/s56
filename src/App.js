// import { Fragment } from 'react'
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Landing from'./components/Landing';
import Login from'./pages/Login';
import Register from './pages/Register';
import './App.css';


function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])
  
  return (
<Router>
    <AppNavbar />
    <Container>
      <Switch>
        <Route exact path="/" component={Landing} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
      </Switch>
    </Container>
</Router>
    );
}

export default App;
