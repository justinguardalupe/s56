// Import necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Container } from 'react-bootstrap'

// AppNavbar component
export default function AppNavbar(){
	return (
	<>
	<Navbar fixed="top" />
	  <Navbar bg="dark" variant="dark">
	    <Container>
	      <Navbar.Brand href="/">
	        <img
	          alt=""
	          src="../../images/etc/logo.png"
	          width="30"
	          height="30"
	          className="d-inline-block align-top"
	        />{' '}
	      E-Commerce
	      </Navbar.Brand>
	      <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end" >
	          <Nav className="me-auto">
	            <Nav.Link href="/products" className="ml-3 mr-3">Products</Nav.Link>
	            <Nav.Link href="/faqs" className="ml-3 mr-3">FAQS</Nav.Link>
	            <Nav.Link href="/login" className="ml-3 mr-3">Login</Nav.Link>
	            </Nav>
	      </Navbar.Collapse>
	    </Container>
	  </Navbar>
	</>
	)
}
