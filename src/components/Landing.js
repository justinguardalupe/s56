import Card from 'react-bootstrap/Card'
import { Button, Row, Col } from 'react-bootstrap';

export default function Landing(){
	
		return (
			<Row className="landing">
				<Col className="p-5">
					<h1>Lorem Ipsum</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis.</p>
					<Button variant="outline-secondary">Order Now!</Button>{' '}
				</Col>
			</Row>

		)
}

