import { Form, Button } from 'react-bootstrap';
import { useState,useEffect,useContext } from 'react';
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2'

export default function Register(){
  const { user } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobile, setMobile] = useState('');
  

  console.log(email);
  console.log(password1);
  console.log(password2);

  function registerUser(e){
    e.preventDefault();
    fetch('http://localhost:4000/users/checkEmail',{
      method: 'POST',
      headers:{
          'Content-Type':'application/json'
      },
      body: JSON.stringify({
          email:email
      })
    })
    .then(res => res.json())
    .then(data => {

      if(data === false){
        fetch('http://localhost:4000/users/register',{
          method: 'POST',
          headers:{
              'Content-Type':'application/json'
          },
          body: JSON.stringify({
              firstName:firstName,
              lastName:lastName,
              email:email,
              password:password1,
              mobileNo:mobile
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(`${data}dataDATA`);
          Swal.fire({
              title:"Registration Successful",
              icon:"success",
              text:"Welcome to Zuitt"
          });
          <Redirect to="/login" />
        }) 

      }
      else{
        Swal.fire({
            title:"Duplicate email found",
            icon:"error",
            text:"Please provide different email"
        })
      }
    })

    setEmail('');
    setPassword2('');
    setPassword1('');
    setFirstName('');
    setLastName('');
    setMobile('');

  }

  useEffect(() => {
    if((firstName !== '' &&lastName !== '' &&mobile !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobile.length === 11))
    {
      setIsActive(true);
    }
    else{
      setIsActive(false);
    }
  }, [firstName,lastName,mobile,email,password1,password2])


  return (
    (user.id !== null) ?
        <Redirect to="/courses" />
    :
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Label><h2>Register</h2></Form.Label>
      <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control 
          type="text" 
          placeholder="Enter First Name" 
          value={firstName}
          onChange = {e => setFirstName(e.target.value)}
          required />
      </Form.Group>
      <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control 
          type="text" 
          placeholder="Enter Last Name" 
          value={lastName}
          onChange = {e => setLastName(e.target.value)}
          required />
      </Form.Group>
      <Form.Group controlId="userEmail" className="mb-3 mt-2">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
          type="email" 
          placeholder="Enter email" 
          value={email}
          onChange = {e => setEmail(e.target.value)}
          required />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>
      <Form.Group controlId="mobile" className="mb-3 mt-2">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control 
          type="text" 
          placeholder="Enter Mobile Number" 
          value={mobile}
          onChange = {e => setMobile(e.target.value)}
          required />
        <Form.Text className="text-muted">
          Input 11 digit number. (09XXXXXXXXX)
        </Form.Text>
      </Form.Group>
      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Password" 
          value={password1}
          onChange={e => setPassword1(e.target.value)}
          required />
      </Form.Group>

      <Form.Group controlId="password2" className="mb-4">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control 
          type="password" 
          placeholder="Verify Password" 
          value={password2}
          onChange={e => setPassword2(e.target.value)}
          required />
      </Form.Group>
      { isActive ?
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
        :
        <Button variant="danger" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      }
    </Form>
  )
}